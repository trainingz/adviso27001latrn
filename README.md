
![](g/i.jpg)

# Regarding Repository 

This repository contains the images that have been generated during the learninf of the [ISO 27001 Lead Auditor Course fromn advisera](https://training.advisera.com/iso-27001-training/)

To understand the context of this git lab repository please see the Deck. Note the images that have been generated are actually put into the deck.

## Deck Made



# GFX 

These are gfx which was created during the training sessions 

| # | Description | Link |
| -- | -- | -- |
| 1 | ISO 27001 Main Philosophy | https://gitlab.com/trainingz/adviso27001latrn/snippets/1858053 |
| 2 | ISO 27001 Safeguards | https://gitlab.com/trainingz/adviso27001latrn/snippets/1858305 |
| 3 | ISMS - Information Security Managment Systems | https://gitlab.com/trainingz/adviso27001latrn/snippets/1858312 |
| 4 | ISO Certification Process | https://gitlab.com/trainingz/adviso27001latrn/snippets/1858324 |
